package com.propertycapsule;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Webutils {
	
	public static WebDriver driver;
	public ReadProperties RP = new ReadProperties();

	// To Launch Browser
	protected void LaunchBrowser(String browser) {
		switch (browser) {
		case "chrome": {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			break;
		}
		case "firefox": {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			break;

		}
		default:
			break;
		}
	}

	// To Launch URL
	protected void getURL() {
		try {
			driver.get(RP.readgetproperty("URL"));
		} catch (Exception e) {
			System.out.println(e);
		}
	}


	// Wait Statement
	protected void waitForPageLoad(int time) {
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}


}
