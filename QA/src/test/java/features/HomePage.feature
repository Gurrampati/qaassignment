Feature: PropertyCapsule HomePage

@SanityTest
Scenario: Enterprise admin login
When user is on the home page
Then click on Enterprise admin login button
And admin login form is displayed

@SanityTest
Scenario: DealMaker signup/login
When user is on the homepage
Then click on deal maker signup button
And signup page is displayed

@SanityTest
Scenario: DealMaker signup/login
When user is on homepage
Then click on map maker tab
And maps page is displayed
