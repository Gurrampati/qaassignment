package com.PageObject;

import org.openqa.selenium.By;

import com.propertycapsule.Webutils;

public class HomePage extends Webutils{
	
	By loginBtn = By.linkText("Enterprise Admin Login");
	By dealMakersingup = By.linkText("Deal Maker Signup/Login");
	By mapMakerTab = By.id("market-btn");
	By tourbooks = By.id("manage-btn");
	
	public void login_button() {
		driver.findElement(loginBtn).click();
	}
	
	public void dealMaker_Signup() {
		driver.findElement(dealMakersingup).click();
	}
	
	public void mapMakerTab() {
		driver.findElement(mapMakerTab);
	}
	
	public void tourBooksTab() {
		driver.findElement(tourbooks);
	}
}
