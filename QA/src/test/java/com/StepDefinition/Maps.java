package com.StepDefinition;

import com.PageObject.HomePage;
import com.propertycapsule.Webutils;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Maps extends Webutils{
	
	HomePage h = new HomePage();
	String homePageTitle = driver.getTitle();
	
	@When("^user is on homepage$")
    public void user_is_on_homepage() throws Throwable {
		System.out.println("User is navigated to the home page");
    }

    @Then("^click on map maker tab$")
    public void click_on_map_maker_tab() throws Throwable {
        h.mapMakerTab();
        Thread.sleep(2000);
    }

    @And("^maps page is displayed$")
    public void maps_page_is_displayed() throws Throwable {
    	String mapsPageTitle = driver.getTitle();
		if (!homePageTitle.equals(mapsPageTitle)) {
			System.out.println("The user is directed to the maps page");

		}
    }

}
