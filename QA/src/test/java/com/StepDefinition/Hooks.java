package com.StepDefinition;

import com.propertycapsule.Webutils;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends Webutils{
	
	@Before("@SanityTest")
	public void browser_launch() throws Throwable {

		// Browser Launch
		LaunchBrowser("chrome");
		driver.manage().window().maximize();
		getURL();
		Thread.sleep(3000);
	}

	@After("@SanityTest")
	public void browser_Close() {
		driver.close();
	}

}
