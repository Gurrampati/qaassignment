package com.StepDefinition;

import com.PageObject.HomePage;
import com.propertycapsule.Webutils;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DealMakerSignup extends Webutils{
	
	HomePage h = new HomePage();
	String homePageTitle = driver.getTitle();
	
	@When("^user is on the homepage$")
    public void user_is_on_the_home_page() throws Throwable {
		
		System.out.println("User is navigated to the home page");
    }

    @Then("^click on deal maker signup button$")
    public void click_on_deal_maker_signup_button() throws Throwable {
        h.dealMaker_Signup();
        Thread.sleep(1000);
    }

    @And("^signup page is displayed$")
    public void signup_page_is_displayed() throws Throwable {
    	String signupPageTitle = driver.getTitle();
		if (!homePageTitle.equals(signupPageTitle)) {
			System.out.println("The user is directed to the Signup page");

		}

    }

}
