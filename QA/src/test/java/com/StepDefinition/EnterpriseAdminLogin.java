package com.StepDefinition;

import com.PageObject.HomePage;
import com.propertycapsule.Webutils;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EnterpriseAdminLogin extends Webutils {

	HomePage h = new HomePage();
	String homePageTitle = driver.getTitle();

	@When("^user is on the home page$")
	public void user_is_on_the_home_page() throws Throwable {
		
		System.out.println("User is navigated to the home page");
		
	}

	@Then("^click on Enterprise admin login button$")
	public void click_on_enterprise_admin_login_button() throws Throwable {
		Thread.sleep(2000);
		h.login_button();

	}

	@And("^admin login form is displayed$")
	public void admin_login_form_is_displayed() throws Throwable {
		String loginPageTitle = driver.getTitle();
		if (!homePageTitle.equals(loginPageTitle)) {
			System.out.println("The user is directed to the login page");

		}
		
		
	}

}
